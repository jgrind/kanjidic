# kanjidic

A redistribution of the [KANJIDIC2] kanji dictionary.

## License

See [`LICENSE`] for more information.

[`LICENSE`]: LICENSE
[KANJIDIC2]: http://www.edrdg.org/wiki/index.php/KANJIDIC_Project
